package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.dto.Products;

/**
 * @author Daniel
 *
 */
public interface IProductsDAO extends JpaRepository<Products, Integer> {
	
}