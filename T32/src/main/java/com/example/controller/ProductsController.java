package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.Products;
import com.example.service.ProductsServiceImpl;

@RestController
@RequestMapping("/api")
public class ProductsController {
	
	@Autowired
	ProductsServiceImpl productServiceImpl;
	
	@GetMapping("/products")
	public List<Products> listProducts() {
		
		return productServiceImpl.listProducts();
		
	}
	
	@PostMapping("/products")
	public Products saveProduct(@RequestBody Products product) {
		
		return productServiceImpl.saveProduct(product);
		
	}
	
	@GetMapping("/products/{id}")
	public Products productXID(@PathVariable(name="id") int id) {
		
		Products Product_xid = new Products();
		
		Product_xid = productServiceImpl.productXID(id);
		
		System.out.println("Product XID: " + Product_xid);
		
		return Product_xid;
		
	}
	
	@PutMapping("/products/{id}")
	public Products updateProduct(@PathVariable(name = "id") int id, @RequestBody Products product) {
		
		Products Product_selected = new Products();
		Products Product_updated = new Products();
		
		Product_selected = productServiceImpl.productXID(id);
		
		Product_selected.setName(product.getName());
		Product_selected.setDetail(product.getDetail());
		Product_selected.setCreated_at(product.getCreated_at());
		Product_selected.setUpdated_at(product.getUpdated_at());
		
		Product_updated = productServiceImpl.updateProduct(Product_selected);
		
		System.out.println("The updated product is: " + Product_updated);
		
		return Product_updated;
		
	}
	
	@DeleteMapping("/products/{id}")
	public void deleteProduct(@PathVariable(name = "id") int id) {
		
		productServiceImpl.deleteProduct(id);
		
	}
	
}