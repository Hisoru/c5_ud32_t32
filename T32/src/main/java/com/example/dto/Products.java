package com.example.dto;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="products")
public class Products {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "detail")
	private String detail;
	
	@Column(name = "created_at")
	@Temporal(TemporalType.DATE)
	private Calendar created_at;
	
	@Column(name = "updated_at")
	@Temporal(TemporalType.DATE)
	private Calendar updated_at;
	
	public Products() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param detail
	 * @param created_at
	 * @param updated_at
	 */
	public Products(int id, String name, String detail, Calendar created_at, Calendar updated_at) {
		
		super();
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.created_at = created_at;
		this.updated_at = updated_at;
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		
		return id;
		
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		
		this.id = id;
		
	}

	/**
	 * @return the name
	 */
	public String getName() {
		
		return name;
		
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	/**
	 * @return the detail
	 */
	public String getDetail() {
		
		return detail;
		
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		
		this.detail = detail;
		
	}
	
	/**
	 * @return the created_at
	 */
	public Calendar getCreated_at() {
		
		return created_at;
		
	}

	/**
	 * @param created_at the created_at to set
	 */
	public void setCreated_at(Calendar created_at) {
		
		this.created_at = created_at;
		
	}
	
	/**
	 * @return the updated_at
	 */
	public Calendar getUpdated_at() {
		
		return updated_at;
		
	}

	/**
	 * @param updated_at the updated_at to set
	 */
	public void setUpdated_at(Calendar updated_at) {
		
		this.updated_at = updated_at;
		
	}

	@Override
	public String toString() {
		
		return "Products [id=" + id + ", name=" + name + ", detail=" + detail + ", created_at=" + created_at
				+ ", updated_at=" + updated_at + "]";
		
	}
		
}