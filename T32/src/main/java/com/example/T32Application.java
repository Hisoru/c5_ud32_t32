package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T32Application {

	public static void main(String[] args) {
		
		SpringApplication.run(T32Application.class, args);
		
	}

}