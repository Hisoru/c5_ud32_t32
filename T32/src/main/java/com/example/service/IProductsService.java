package com.example.service;

import java.util.List;

import com.example.dto.Products;

public interface IProductsService {

	public List<Products> listProducts();
	
	public Products saveProduct(Products product);
	
	public Products productXID(int id);
	
	public Products updateProduct(Products product);
	
	public void deleteProduct(int id);

}