package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.IProductsDAO;
import com.example.dto.Products;

@Service
public class ProductsServiceImpl implements IProductsService {

	@Autowired
	IProductsDAO iProductsDAO;
	
	@Override
	public List<Products> listProducts() {

		return iProductsDAO.findAll();
		
	}

	@Override
	public Products saveProduct(Products product) {

		return iProductsDAO.save(product);
		
	}

	@Override
	public Products productXID(int id) {

		return iProductsDAO.findById(id).get();
		
	}

	@Override
	public Products updateProduct(Products product) {

		return iProductsDAO.save(product);
		
	}

	@Override
	public void deleteProduct(int id) {

		iProductsDAO.deleteById(id);
		
	}

}